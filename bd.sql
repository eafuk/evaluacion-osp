CREATE DATABASE cine;

CREATE TABLE cine.pelicula
(
id int primary key auto_increment,
nombre varchar(150),
fecha_publicacion datetime,
estado int,
usu_registro varchar(150),
fec_registro datetime,
usu_modif varchar(150),
fec_modif datetime
);


CREATE TABLE cine.turno
(
id int primary key auto_increment,
descripcion time,
estado int,
usu_registro varchar(150),
fec_registro datetime,
usu_modif varchar(150),
fec_modif datetime
);