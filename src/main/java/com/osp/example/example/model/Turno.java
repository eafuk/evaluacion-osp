package com.osp.example.example.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "turno")
public class Turno {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "descripcion")
	@JsonFormat(pattern = "hh:mm:ss")
	private String descripcion;

	@Column(name = "estado")
	private Integer estado;

	@Column(name = "usu_registro")
	private String usuRegistro;

	@Column(name = "fec_registro")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date fecRegistro;

	@Column(name = "usu_modif")
	private String usuModif;

	@Column(name = "fec_modif")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date fecModif;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getUsuRegistro() {
		return usuRegistro;
	}

	public void setUsuRegistro(String usuRegistro) {
		this.usuRegistro = usuRegistro;
	}

	public Date getFecRegistro() {
		return fecRegistro;
	}

	public void setFecRegistro(Date fecRegistro) {
		this.fecRegistro = fecRegistro;
	}

	public String getUsuModif() {
		return usuModif;
	}

	public void setUsuModif(String usuModif) {
		this.usuModif = usuModif;
	}

	public Date getFecModif() {
		return fecModif;
	}

	public void setFecModif(Date fecModif) {
		this.fecModif = fecModif;
	}

}
