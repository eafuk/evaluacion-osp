package com.osp.example.example.model;

public class RespuestaJson {

	private String tipo;

	private String mensaje;

	private Object data;

	public RespuestaJson() {
		super();
	}

	public RespuestaJson(String tipo, String mensaje, Object data) {
		super();
		this.tipo = tipo;
		this.mensaje = mensaje;
		this.data = data;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
