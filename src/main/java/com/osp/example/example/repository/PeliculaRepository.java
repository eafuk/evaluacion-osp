package com.osp.example.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.osp.example.example.model.Pelicula;

public interface PeliculaRepository extends JpaRepository<Pelicula, Integer> {

	public List<Pelicula> findByEstadoNot(Integer estado);
}
