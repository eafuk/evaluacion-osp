package com.osp.example.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.osp.example.example.model.RespuestaJson;
import com.osp.example.example.model.Turno;
import com.osp.example.example.service.TurnoService;
import com.osp.example.example.utils.ConstantesEnum;

@RestController
public class TurnoController {

	@Autowired
	private TurnoService turnoService;

	@PostMapping(path = "/registroturno")
	public RespuestaJson registro(@RequestBody Turno turno) {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			turno = turnoService.registro(turno);

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(turno);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}

	}

	@PostMapping(path = "/actualizarturno")
	public RespuestaJson actualizar(@RequestBody Turno turno) {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			turnoService.actualizar(turno);

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}
	}

	@PostMapping(path = "/eliminarturno")
	public RespuestaJson eliminar(@RequestBody Turno turno) {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			turnoService.eliminar(turno);

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}
	}

	@GetMapping(path = "/obtenerturno")
	public RespuestaJson obtener() {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			List<Turno> turnos = turnoService.obtener();

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(turnos);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}
	}

}
