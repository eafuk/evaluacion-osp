package com.osp.example.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.osp.example.example.model.Pelicula;
import com.osp.example.example.model.RespuestaJson;
import com.osp.example.example.service.PeliculaService;
import com.osp.example.example.utils.ConstantesEnum;

@RestController
public class PeliculaController {

	@Autowired
	private PeliculaService peliculaService;

	@PostMapping(path = "/registropeli")
	public RespuestaJson registro(@RequestBody Pelicula pelicula) {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			pelicula = peliculaService.registro(pelicula);

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(pelicula);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}
	}

	@PostMapping(path = "/actualizarpeli")
	public RespuestaJson actualizar(@RequestBody Pelicula pelicula) {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			peliculaService.actualizar(pelicula);

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}
	}

	@PostMapping(path = "/eliminarpeli")
	public RespuestaJson eliminar(@RequestBody Pelicula pelicula) {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			peliculaService.eliminar(pelicula);

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}
	}

	@GetMapping(path = "/obtenerpeli")
	public RespuestaJson obtener() {

		RespuestaJson respuesta = new RespuestaJson();

		try {

			List<Pelicula> peliculas = peliculaService.obtener();

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.SUCCESS.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.SUCCESS.getDescripcion());
			respuesta.setData(peliculas);
			return respuesta;
		} catch (Exception e) {

			respuesta.setTipo(ConstantesEnum.TipoRespuesta.ERROR.getDescripcion());
			respuesta.setMensaje(ConstantesEnum.MensajeGenerico.ERROR.getDescripcion());
			respuesta.setData(null);
			return respuesta;
		}
	}
}
