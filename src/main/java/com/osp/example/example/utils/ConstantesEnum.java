package com.osp.example.example.utils;

public class ConstantesEnum {

	public enum Estados {

		ACTIVO(1), INACTIVO(2), ELIMINADO(3);

		private int id;

		private Estados(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

	}

	public enum TipoRespuesta {

		SUCCESS("Success"), ERROR("Error");

		private String descripcion;

		private TipoRespuesta(String descripcion) {
			this.descripcion = descripcion;
		}

		public String getDescripcion() {
			return descripcion;
		}

	}

	public enum MensajeGenerico {

		SUCCESS("El proceso se completo satisfactoriamente"), ERROR("El proceso no se pudo completar");

		private String descripcion;

		private MensajeGenerico(String descripcion) {
			this.descripcion = descripcion;
		}

		public String getDescripcion() {
			return descripcion;
		}

	}
}
