package com.osp.example.example.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osp.example.example.model.Pelicula;
import com.osp.example.example.repository.PeliculaRepository;
import com.osp.example.example.service.PeliculaService;
import com.osp.example.example.utils.ConstantesEnum;

@Service
public class PeliculaServiceImpl implements PeliculaService {

	@Autowired
	private PeliculaRepository peliculaRepository;

	@Override
	@Transactional
	public Pelicula registro(Pelicula pelicula) throws Exception {
		pelicula.setFecRegistro(new Date());
		return peliculaRepository.save(pelicula);
	}

	@Override
	@Transactional
	public void actualizar(Pelicula pelicula) throws Exception {
		Pelicula peliculaBD = peliculaRepository.getOne(pelicula.getId());
		peliculaBD.setEstado(pelicula.getEstado());
		peliculaBD.setFechaPublicacion(pelicula.getFechaPublicacion());
		peliculaBD.setUsuRegistro(pelicula.getUsuRegistro());
		peliculaBD.setFecRegistro(pelicula.getFecRegistro());
		peliculaBD.setUsuModif(pelicula.getUsuModif());
		peliculaBD.setFecModif(new Date());
		peliculaRepository.save(peliculaBD);
	}

	@Override
	@Transactional
	public void eliminar(Pelicula pelicula) throws Exception {
		Pelicula peliculaBD = peliculaRepository.getOne(pelicula.getId());
		peliculaBD.setUsuModif(pelicula.getUsuModif());
		peliculaBD.setFecModif(new Date());
		peliculaBD.setEstado(ConstantesEnum.Estados.ELIMINADO.getId());
		peliculaRepository.save(peliculaBD);
	}

	@Override
	public List<Pelicula> obtener() throws Exception {
		return peliculaRepository.findByEstadoNot(ConstantesEnum.Estados.ELIMINADO.getId());
	}

}
