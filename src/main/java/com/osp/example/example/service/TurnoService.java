package com.osp.example.example.service;

import java.util.List;

import com.osp.example.example.model.Turno;

public interface TurnoService {

	public Turno registro(Turno turno) throws Exception;

	public void actualizar(Turno turno) throws Exception;

	public void eliminar(Turno turno) throws Exception;

	public List<Turno> obtener() throws Exception;
}
