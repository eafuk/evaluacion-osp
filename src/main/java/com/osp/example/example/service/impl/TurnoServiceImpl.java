package com.osp.example.example.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osp.example.example.model.Pelicula;
import com.osp.example.example.model.Turno;
import com.osp.example.example.repository.TurnoRepository;
import com.osp.example.example.service.TurnoService;
import com.osp.example.example.utils.ConstantesEnum;

@Service
public class TurnoServiceImpl implements TurnoService {

	@Autowired
	private TurnoRepository turnoRepository;

	@Override
	@Transactional
	public Turno registro(Turno turno) throws Exception {
		turno.setFecRegistro(new Date());
		return turnoRepository.save(turno);
	}

	@Override
	@Transactional
	public void actualizar(Turno turno) throws Exception {
		Turno turnoBD = turnoRepository.getOne(turno.getId());
		turnoBD.setEstado(turno.getEstado());
		turnoBD.setUsuRegistro(turno.getUsuRegistro());
		turnoBD.setFecRegistro(turno.getFecRegistro());
		turnoBD.setUsuModif(turno.getUsuModif());
		turnoBD.setFecModif(new Date());
		turnoRepository.save(turno);
	}

	@Override
	@Transactional
	public void eliminar(Turno turno) throws Exception {
		Turno turnoBD = turnoRepository.getOne(turno.getId());
		turnoBD.setUsuModif(turno.getUsuModif());
		turnoBD.setFecModif(new Date());
		turnoBD.setEstado(ConstantesEnum.Estados.ELIMINADO.getId());
		turnoRepository.save(turno);
	}

	@Override
	public List<Turno> obtener() throws Exception {
		return turnoRepository.findByEstadoNot(ConstantesEnum.Estados.ELIMINADO.getId());
	}

}
