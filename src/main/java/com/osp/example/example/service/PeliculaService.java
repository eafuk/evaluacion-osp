package com.osp.example.example.service;

import java.util.List;

import com.osp.example.example.model.Pelicula;

public interface PeliculaService {

	public Pelicula registro(Pelicula pelicula) throws Exception;

	public void actualizar(Pelicula pelicula) throws Exception;

	public void eliminar(Pelicula pelicula) throws Exception;

	public List<Pelicula> obtener() throws Exception;
}
